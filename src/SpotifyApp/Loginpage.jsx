import React, { Fragment } from 'react';
import './login.css';
import { NavLink } from 'react-router-dom';



const Loginpage = () => {
  return (
    <Fragment>
	<div className="bg-form">
        <div className="form01">
            <h2>LogIn Here</h2>
            <input type="email" name="" id="" placeholder="Enter Email Here" />
            <input type="password" name="" id="pass-field" placeholder="Enter Password Here" />
            <button className="btn01">Submit</button>
            <p className='text-white'>Don't have an Account<br/><NavLink to="/login/signup">create account here</NavLink></p>
        </div>
      </div>
    </Fragment>
  )
}

export default Loginpage