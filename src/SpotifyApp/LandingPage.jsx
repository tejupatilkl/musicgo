import React, { Fragment } from 'react'
import{BrowserRouter,Route,Routes} from 'react-router-dom';
import Loginpage from './Loginpage';
import Mainpage from './Mainpage';
import NavBar from './Navbar';
import Signup from './Signup';
import SongsPage from './SongsPage';


const LandingPage = () => {
  return (
    <Fragment>
    <NavBar/>
          <Routes>
              <Route path="/" element={<Mainpage/>} />
              <Route path="/login" element={<Loginpage/>} />
              <Route path="/login/signup" element={<Signup/>} />
              <Route path="/songs" element={<SongsPage/>} />
             
          </Routes>  
    </Fragment>
  )
}

export default LandingPage