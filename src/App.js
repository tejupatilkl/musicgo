/** @format */

import React from "react";
import "./App.css";
import { BrowserRouter } from "react-router-dom";
import LandingPage from "./SpotifyApp/LandingPage";

function App() {
    return (
        <React.Fragment>
            <BrowserRouter>
                <LandingPage />
            </BrowserRouter>
        </React.Fragment>
    );
}

export default App;
