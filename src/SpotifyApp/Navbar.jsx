import React from 'react'
import { NavLink, Link } from 'react-router-dom';

function NavBar () {

  return (
    <React.Fragment>
        <nav className="navbar navbar-dark navbar-expand-sm m-0" style={{"background":"black"}}>
                <div className="container">
                    <h1 className="navbar-brand">Spotify</h1>
                    <div>
                        <ul className="navbar-nav">
                            <li className="nav-item nav-link hover text-white"><NavLink to='/'>Premium</NavLink></li>
                            <li className="nav-item nav-link hover text-white"><NavLink to='/'>Support</NavLink></li>
                            <li className="nav-item nav-link hover text-white"><NavLink to='/'>Download</NavLink></li>
                            <li className="nav-item nav-link hover text-white"><NavLink to='/login'>Login</NavLink></li>
                        </ul>
                    </div>
                </div>
            </nav>
    </React.Fragment>
  )
}

export default NavBar