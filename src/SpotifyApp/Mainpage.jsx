import React from 'react';
import { NavLink } from 'react-router-dom';

const Mainpage = () => {
  return (
    <React.Fragment>
    <div className='backgroundImage mt-0 ml-4'>
    <div className='MainContentFirst ml-4'>
      <br/><br/>
    <div className='text-white m-5'><h1>Play millions of songs</h1>
    <h1> and podcasts, for free.</h1>
    <button className="button ml-4"><NavLink to='/songs'>Get Spotify Free</NavLink></button>
    </div>
    </div>
    </div>
    </React.Fragment>
  )
}

export default Mainpage